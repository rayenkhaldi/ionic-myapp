import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { __await } from 'tslib';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form : FormGroup;


  constructor(private authService:AuthService,
    private router:Router,
    private alertCtrl:AlertController) {}

  ngOnInit() {
  }
  async loginUser(form):Promise<void>
  {
    this.authService.loginUser(form.value.email, form.value.password).
    then(
      ()=>{
        if (form.value.email=='rayenkhaldi@gmail.com') {
          this.router.navigateByUrl('home');
        }else{
          this.router.navigateByUrl('home');

        }
      },
      async error =>{
        const alert = await this.alertCtrl.create({
          message:error.message,
          buttons:[{text:'ok',role:'cancel'}],
          
        });
        await alert.present();
      }

    );
  }

    goToReset(){
      this.router.navigateByUrl('password-reset');
    }
    goTosignUp(){
      this.router.navigateByUrl('sign-up');
    }
}

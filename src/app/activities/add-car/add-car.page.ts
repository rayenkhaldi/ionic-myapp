import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { FirebaseService } from 'src/app/services/firebase.service';


interface CarsData{
  carburant: number;
  latitude: string;
  longitude: string;
  matricule: string;
  name: string;
  typeCarburant: string;
}

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.page.html',
  styleUrls: ['./add-car.page.scss'],
})
export class AddCarPage implements OnInit {
  carsList = [];
  carsData: CarsData;
  carsForm: FormGroup;


  constructor(public formBuilder : FormBuilder,private firebaseService: FirebaseService,public navCtrl:NavController ) { 
    this.carsData = {} as CarsData;

  }

  ngOnInit() {
    this.carsForm = this.formBuilder.group({
      Name: ['', [Validators.required]],
      Matricule: ['', [Validators.required]],
      typeCarburant: ['', [Validators.required]],
      Carburant: ['', [Validators.required]],
      Latitude: ['', [Validators.required]],
      Longitude: ['', [Validators.required]]
    })
    this.firebaseService.showList().subscribe(data => {
      this.carsList = data.map(e => {
        return {
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Matricule: e.payload.doc.data()['Matricule'],
          typeCarburant: e.payload.doc.data()['typeCarburant'],
          Carburant: e.payload.doc.data()['Carburant'],
          Latitude: e.payload.doc.data()['Latitude'],
          Longitude: e.payload.doc.data()['Longitude'],
        };
      })
      console.log(this.carsList);
    });
    
  }
  Create() {
    console.log(this.carsForm.value);
    if (this.carsForm.valid) {
      this.firebaseService.createCar(this.carsForm.value).then(resp => {
        this.carsForm.reset();
      })
        .catch(error => {
          console.log(error);
        });
      
    }
    
    this.navCtrl.navigateForward("home");
      
    
  }


}

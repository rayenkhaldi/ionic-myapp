import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.page.html',
  styleUrls: ['./cars.page.scss'],
})
export class CarsPage implements OnInit {
  carsList = [];
  constructor(private firebaseService : FirebaseService) { }

  ngOnInit() {
    this.firebaseService.showList().subscribe(data => {
      this.carsList = data.map(e => {
        return {
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Matricule: e.payload.doc.data()['Matricule'],
          typeCarburant: e.payload.doc.data()['typeCarburant'],
          Carburant: e.payload.doc.data()['Carburant'],
          Latitude: e.payload.doc.data()['Latitude'],
          Longitude: e.payload.doc.data()['Longitude'],
        };
      })
      
    });
  }
  Ajouter(Carinf) {
    Carinf.isEdit = true;
    Carinf.EditName = Carinf.Name;
    Carinf.EditCarburant= Carinf.Carburant;
    Carinf.EdittypeCarburant= Carinf.typeCarburant;
    Carinf.EditMatricule= Carinf.Matricule;
    Carinf.EditLatitude = Carinf.Latitude;
    Carinf.EditLongitude = Carinf.Longitude;
  }
  Supprimer(id) {
    this.firebaseService.deleteCar(id);
  }
  Update(Car) {
    let Carinf = {};
    Carinf['Name'] = Car.EditName;
    Carinf['Carburant'] = Car.EditCarburant;
    Carinf['typeCarburant'] = Car.EdittypeCarburant;
    Carinf['Matricule'] = Car.EditMatricule;
    Carinf['Latitude'] = Car.EditLatitude;
    Carinf['Longitude'] = Car.EditLongitude;
  
    
    
    this.firebaseService.updateCar(Car.id, Carinf);
    Car.isEdit = false;
  }

 

}

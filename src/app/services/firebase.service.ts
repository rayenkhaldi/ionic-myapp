import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  collectionName='Cars';
  firebaseService: any;

  constructor(private firestore: AngularFirestore) { }


createCar(id){
  return this.firestore.collection(this.collectionName).add(id);
}

showList(){
  return this.firestore.collection(this.collectionName).snapshotChanges();
}

updateCar(id,element){
  this.firestore.doc(this.collectionName + '/' +id).update(element);
}

deleteCar(id){
 this.firestore.doc(this.collectionName + '/' +id).delete();

}
  
}
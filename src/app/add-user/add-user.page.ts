import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  form:FormGroup;

  constructor(private formBuilder :FormBuilder,public userService:UserService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      username: ['', [Validators.required, Validators.min(0)]],
      email: ['', [Validators.required, Validators.min(0)]],
      password: ['', [Validators.required, Validators.min(0)]]
    });

  }

  submit() {
    this.userService.addUser(this.form.value).subscribe((result) => {
      alert("User Added successfully");
    });

  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  header : HttpHeaders;
  constructor(public http: HttpClient) { console.log('Test servciee http!!!!!');}

  

  addUser(user) {

    let url = '/elasticusers/user';
    this.header = new HttpHeaders();
    this.header.append("content.type", "application/json");
    return this.http.post(url, user, { headers: this.header });
  }
  showallusers() {

    let url = '/elasticusers/user/_search';
    this.header = new HttpHeaders();
    this.header.append("content.type", "application/json");
    return this.http.get(url, { headers: this.header });

  }
}

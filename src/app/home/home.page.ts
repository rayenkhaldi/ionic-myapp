import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, AnimationKeyFrames } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { FirebaseService } from '../services/firebase.service';




declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  map: any;
  @ViewChild('map',{read:ElementRef,static:false}) mapRef:ElementRef;
  carsList = [];
  infoWindows: any = [];



  constructor(private authService: AuthService, private router: Router, private alertCtrl: AlertController, private firebaseService: FirebaseService) {
    
  }
  ngOnInit() {
  
    this.firebaseService.showList().subscribe(data => {
      this.carsList = data.map(e => {
        return {
          id: e.payload.doc.id,
          Name: e.payload.doc.data()['Name'],
          Matricule: e.payload.doc.data()['Matricule'],
          typeCarburant: e.payload.doc.data()['typeCarburant'],
          Carburant: e.payload.doc.data()['Carburant'],
          Latitude: e.payload.doc.data()['Latitude'],
          Longitude: e.payload.doc.data()['Longitude'],
        };
      })
      this.addMarkersToMap(this.carsList);
      //console.log(this.carsList);
    });

  }
  ionViewDidEnter() {
    this.showMap();
  }
  showMap() {
    const location = new google.maps.LatLng(35.82441252528545, 10.596610959628691);
    const options = {
      center: location,
      zoom: 15,
      disableDefaultUI: false
    }
    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
  }
  
  addMarkersToMap(carsList) {
    for (let car of carsList) {
      // console.log(carsList);
      //console.log("ffffffffffff");

      let position = new google.maps.LatLng(car.Latitude, car.Longitude);
      let mapMarker = new google.maps.Marker({
        position: position,
        title: car.Name,
        matricule: car.Matricule,
        carburant: car.Carburant+'%'
      });
      mapMarker.setMap(this.map);
      this.addInfoWindowToMarker(mapMarker);
      console.log(mapMarker);

    }
    console.log(carsList);

  }

  addInfoWindowToMarker(marker) {
    let infoWindowContent = '<div id="content">' +
      '<h2 id="firstHeading" class"firstHeading">' + marker.title + '</h2>' +
      '<p>Matricule: ' + marker.matricule + '</p>' +
      '<p>Carburant: ' + marker.carburant + '</p>' +
      '</div>';

    let infoWindow = new google.maps.InfoWindow({
      content: infoWindowContent
    });

    marker.addListener('click', () => {
      this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });
    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for (let window of this.infoWindows) {
      window.close();
    }
  }

  

   async logOut(): Promise<void> {
     this.authService.logOutUser().
       then(
         () => {
           this.router.navigateByUrl('login');

         },
         async error => {
           const alert = await this.alertCtrl.create({
             message: error.message,
             buttons: [{ text: 'ok', role: 'cancel' }],

           });
           await alert.present();
         }

       );
   }


}

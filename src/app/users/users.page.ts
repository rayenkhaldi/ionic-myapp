import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  users: any

  constructor(public userService : UserService ) { }

  ngOnInit() {
    this.userService.showallusers().subscribe((reponse) => { this.users = reponse["hits"]["hits"] });
    // or reponse.hits.hits
  }

}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OperatorService {

  constructor() { }

  calcul(op){
    let result = 0;
    switch (op.op) {
      case '*':
        result = parseInt(op.n1) * parseInt(op.n2);

        break;

      default:
        result = parseInt(op.n1) + parseInt(op.n2);
        break;
    }
    return result;
  }
}

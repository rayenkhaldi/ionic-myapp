import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./activities/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'password-reset',
    loadChildren: () => import('./activities/password-reset/password-reset.module').then( m => m.PasswordResetPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./activities/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'add-car',
    loadChildren: () => import('./activities/add-car/add-car.module').then( m => m.AddCarPageModule)
  },
  {
    path: 'cars',
    loadChildren: () => import('./activities/cars/cars.module').then( m => m.CarsPageModule)
  },
  {
    path: 'add-user',
    loadChildren: () => import('./add-user/add-user.module').then( m => m.AddUserPageModule)
  },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
